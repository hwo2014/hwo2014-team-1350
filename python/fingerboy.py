import collections
from track import TrackPiece
import math
from botlogger import BotLogger

class Fingerboy(object):
    """ A half-witted race engineer and manager in one """
    
    def __init__(self, name, history_length = 3):
        self.name = name
        self.history = collections.deque([], history_length)
        self.braking_powers = collections.deque([], 10)
        self.pieces = None

    def init_race(self, racedata):
        self.message = "???"
        self.last_command = None
        self.trackname = racedata['track']['name']
        self.lanes = racedata['track']['lanes'] #we should ensure that lane.index = list index
        if self.pieces is None: # do not rebuild if we know something about the track already
            self.pieces = []
            prev_piece = None
            for idx, piece in enumerate(racedata['track']['pieces']):
                new_piece = TrackPiece(idx, piece, self.lanes)
                if prev_piece is not None:
                    prev_piece.next_piece = new_piece
                    new_piece.prev_piece = prev_piece
                self.pieces.append(new_piece)
                prev_piece = new_piece
            prev_piece.next_piece = self.pieces[0]
            self.pieces[0].prev_piece = prev_piece
        self.cars = racedata['cars']
        self.longest_straight = self.get_longest_straight()
        self.turbo = None
        if 'laps' in racedata['raceSession']:
            self.lapcount = racedata['raceSession']['laps']
        else:
            self.lapcount = -1

    def piece_changed(self): #TODO: we have this and piece_boundary... argh
        return self.get()['piecePosition']['pieceIndex'] != self.get(since=1)['piecePosition']['pieceIndex']
    
    def current_piece(self, since=0):
        return self.pieces[self.get(since=since)['piecePosition']['pieceIndex']]

    def update(self, msg):
        self.tick = msg.get('gameTick', 0)
        self.history.appendleft(msg['data'])
        self.set_speed()
        if self.last_command == 'brake':
            current_speed = self.get()['speed']
            last_speed = self.get(since=1)['speed']
            if last_speed != 0 and current_speed < last_speed:
                power = current_speed/last_speed
                self.braking_powers.appendleft(power)
        self.last_command = None
        self.update_analyzers()

    def get_braking_power(self):
        if len(self.braking_powers) < 3:
            power = 0.98
        else:
            power = sorted(self.braking_powers)[int(len(self.braking_powers)/2)] 
        return power

    def set_speed(self):
        current, previous = self.get()['piecePosition'], self.get(since=1)['piecePosition']
        current_piece = self.pieces[current['pieceIndex']]
        previous_piece = self.pieces[previous['pieceIndex']]
        
        lane = current['lane']['startLaneIndex']

        current_offs = current['inPieceDistance']
        previous_offs = previous['inPieceDistance']
        total_distance = 0.0
        while previous_piece != current_piece:
            total_distance += previous_piece.lengths[lane] - previous_offs
            previous_offs = 0
            previous_piece = previous_piece.next_piece
        total_distance += current_offs - previous_offs
        self.get()['speed'] = total_distance


    def get(self, name=None, since=0):
        name = name or self.name
        for car in self.history[min(since, len(self.history)-1)]:
            #print car
            if car['id']['name'] == name:
                return car

    def get_delta(self, var, name=None, since=0):
        #print "name: ", name, self.name
        return self.get(name,since)[var] - self.get(name,since+1)[var]

    def get_accel(self, var, name=None):
        # should get ratio instead? problematic though.
        return self.get_delta(var,name,0) - self.get_delta(var,name,1)

    def get_slide(self):
        angle_diff = self.get_delta("angle")
        angle = self.get()["angle"]
        if angle < 0:
            angle_diff = -angle_diff
        return angle, angle_diff

    def piece_boundary(self):
        return self.get()['piecePosition']['pieceIndex'] != self.get(since=1)['piecePosition']['pieceIndex']

    def get_lane(self, since=0):
        return self.get(since=since)['piecePosition']['lane']['endLaneIndex']

    def get_straight_length(self, p):
        straight_length = p.lengths[0] #straights have identical lanes
        while p.next_piece.is_straight:
            p = p.next_piece
            straight_length += p.lengths[0]
        return straight_length

    def get_longest_straight(self):
        starts = (piece.next_piece for piece in self.pieces if piece.next_piece.is_straight and not piece.is_straight)
        lengths = (self.get_straight_length(start) for start in starts)
        return max(lengths)

    def suggest_turbo(self):
        if not self.turbo:
            return False

        p = self.current_piece()
        lane = self.get_lane()
        if not p.is_straight: #TODO: use turbo somewhere if the track has no straights...
            return False

        angle, angle_diff = self.get_slide()
        angle_limiter = angle_diff > 3 or abs(angle) > 40
        if angle_limiter:
            return False

        straight_length = self.get_straight_length(p)
        if straight_length/self.longest_straight > 0.9:
            self.turbo = None
            print "Using turbo, because current straight %.1f vs. %.1f" % (straight_length, self.longest_straight)
            return True
        return False


    def suggest_switch(self):
        # suggest switch in start of a new piece if there is a switch in the next piece
        if not self.piece_boundary():
            return None

        next_piece = self.current_piece().next_piece
        if not next_piece.switch:
            return None

        lane_distances = [0.0 for lane in self.lanes]
        for lane in self.lanes:
            piece = next_piece.next_piece #piece after the switch
            while not piece.switch:
                lane_distances[lane['index']] += piece.lengths[lane['index']]
                piece = piece.next_piece

        best_lane_index = lane_distances.index(min(lane_distances))
        current_lane_index = self.get()['piecePosition']['lane']['endLaneIndex']
        offset_diff = self.lanes[best_lane_index]['distanceFromCenter'] - self.lanes[current_lane_index]['distanceFromCenter']

        if offset_diff > 0:
            return "Right"
        elif offset_diff < 0:
            return "Left"
        else: #same offset
            return None

    def _find_braking_target(self, p, lane, initial_speed):
        bends = []
        dist = p.lengths[lane] - self.get()['piecePosition']['inPieceDistance']
        nb = p
        for x in xrange(3): # take 3 next bends into account
            nb = nb.next_piece
            while nb.is_straight:
                dist += nb.lengths[lane] 
                nb = nb.next_piece
            bends.append((nb, dist))
            dist += nb.lengths[lane]
        
        for bend, bdist in bends:
            speed = initial_speed
            delay_ticks = 1
            pos = 0
            while True:
                if delay_ticks > 0:
                    delay_ticks -= 1
                else:
                    speed *= self.get_braking_power()
                pos += speed
                if speed < bend.target_speeds[lane]:
                    break #test the next bend
                if pos >= bdist:
                    return bend, bdist #WE NEED TO BRAKE HARRRD
        return None, None

    def suggest_throttle(self):
        speed = self.get()['speed']
        angle, angle_diff = self.get_slide()
        lane = self.get_lane()
        p = self.current_piece()

        if speed > p.target_speeds[lane]: #if speeding, brake
            self.message = "Braking for target %.1f" % p.target_speeds[lane]
            self.last_command = 'brake'
            return 0.0

        
        brake_target, brake_distance = self._find_braking_target(p, lane, speed)

        if brake_target and brake_target.index < p.index and self.get()['piecePosition']['lap']+1 == self.lapcount: 
            self.message = "Last straight! GAS GAS GAS!"
            return 1.0

        if brake_target: #bend coming up...
            self.message = "Braking for P%02d, distance = %.1f, target=%.1f, radius=%.1f" % (brake_target.index, brake_distance, brake_target.target_speeds[lane], brake_target.lane_radiuses[lane])
            return 0.0

        # slide angle limiters
        
        if ((angle_diff > 3.6) 
            or (angle_diff > 2 and abs(angle) > 30) 
            or (angle_diff > 1 and abs(angle) > 40)
            or (angle_diff > 0 and abs(angle) > 45)):
            self.message = "Slide LIMIT"
            return 0.0
        else:
            self.message = "Full throttle!"
            return 1.0

    def update_analyzers(self):
        speed = self.get()['speed']
        angle, angle_diff = self.get_slide()
        lane = self.get_lane()
        p = self.current_piece()
        p.analyzers[lane].update(speed, angle, angle_diff)
        if self.piece_boundary():
            last_lane = self.get_lane(since=1)
            self.current_piece(since=1).analyzers[last_lane].analyze()


    def dump_ai(self, prefix):
        out = BotLogger(prefix + "aidump.log")
        for piece in self.pieces:
            if piece.is_straight:
                continue
            for analyzer in piece.analyzers:
                data = analyzer.dump_data()
                if data is not None:
                    out.write(data)
