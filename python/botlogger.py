import codecs
import os.path

class BotLogger(object):
	def __init__(self, fn, printmod=None):
		""" never print to console if printmod is None """
		try:
			self.f = codecs.open(os.path.join('logs', fn), 'w', 'utf-8')
		except IOError:
			print "! Log writing disabled"
			self.f = None
		self.linecount = 0
		self.printmod = printmod

	def write(self, data, priority=False):
		self.linecount += 1
		if self.printmod is not None and (self.linecount % self.printmod == 0 or priority):
			print data
		if self.f is not None:
			self.f.write(data + '\n')

