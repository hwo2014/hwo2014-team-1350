import sys
import socket
from communicator import Communicator
from fingerboy import Fingerboy

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        race = None
        if len(sys.argv) == 6:
            race = sys.argv[5]
        bot = Communicator(s, name, key, log_prefix = race or "")
        ai = Fingerboy(name)
        bot.set_ai(ai)

        bot.run(race=race)
