import math

class LaneAnalyzer(object):
    def __init__(self, piece, lane_id):
        self.piece = piece
        self.lane_id = lane_id
        self.original_target_speed = self.piece.target_speeds[lane_id]
        self.old_angles = None
        self.old_diffs = None
        self.old_speeds = None
        self.clear()

    def clear(self):
        self.slide_angles = []
        self.slide_diffs = []
        self.speeds = []
        self.crashed = False

    def save_history(self, target):
        self.old_angles = self.slide_angles
        self.old_diffs = self.slide_diffs
        self.old_speeds = self.speeds
        self.old_crashed = self.crashed
        self.old_target = target

    def update(self, speed, angle, diff):
        self.slide_angles.append(angle)
        self.slide_diffs.append(diff)
        self.speeds.append(speed)

    def crash(self):
        self.crashed = True
        print " >>> P%02d L%d crashed" % (self.piece.index, self.lane_id)

    def analyzable(self):
        return len(self.speeds) > 0

    def analyze(self):

        if not self.speeds:
            return
        current_target = self.piece.target_speeds[self.lane_id]
        multiplier = 1.0
        if self.crashed:
            multiplier *= 0.8
            print " >>> P%02d L%d setting crash multiplier" % (self.piece.index, self.lane_id)
        else:
            max_angle = max(abs(angle) for angle in self.slide_angles)
            max_diff = max(self.slide_diffs)
            max_speed = max(self.speeds)
            exit_speed = self.speeds[-1]
            if max_speed > current_target: #we hit the target at some point

                if max_angle < 10 and max_diff < 4: # safest
                    multiplier *= 1.1    
                elif max_angle < 20 and max_diff < 3:
                    multiplier *= 1.06
                elif max_angle < 35 and max_diff < 2:
                    multiplier *= 1.03

        new_target = current_target * multiplier
        if new_target > current_target:
            print" >>> P%02d L%d +++ from %.2f to %.2f" % (self.piece.index, self.lane_id, current_target, new_target)
            self.piece.target_speeds[self.lane_id] = new_target
        elif new_target < current_target:
            piece = self.piece
            multi_add = (1-multiplier)/3
            for x in xrange(3):
                for i,spd in enumerate(piece.target_speeds):
                    print" >>> P%02d L%d --- from %.2f to %.2f" % (piece.index, i, spd, spd * multiplier)
                    piece.target_speeds[i] = spd * multiplier
                piece = piece.prev_piece
                multiplier += multi_add


        self.save_history(current_target)
        self.clear()
        #print " >>> P%02d L%d analyzed" % (self.piece.index, self.lane_id)

    def dump_data(self):
        if not self.old_speeds:
            return None
        max_angle = max(abs(angle) for angle in self.old_angles)
        max_diff = max(self.old_diffs)
        max_speed = max(self.old_speeds)
        enter_speed = self.old_speeds[0]
        exit_speed = self.old_speeds[-1]
        if self.piece.is_straight:
            lane_radius = 0
        else:
            lane_radius = self.piece.lane_radiuses[self.lane_id]
        return "P%02d-L%d radius=%03d   %.2f -> %.2f (%.2f %.2f %.2f)   angle=%.2f %.2f" % ( 
                                        self.piece.index, 
                                        self.lane_id, 
                                        lane_radius,
                                        self.original_target_speed,
                                        self.piece.target_speeds[self.lane_id],
                                        enter_speed,
                                        max_speed,
                                        exit_speed,
                                        max_angle,
                                        max_diff,
                                        )


def lane_length(dist, radius, angle):
    if angle < 0:
        return 2 * math.pi * (radius + dist) * (-angle / 360.0)
    else: 
        return 2 * math.pi * (radius - dist) * (angle / 360.0)

def true_radius(dist, radius, angle):
    if angle < 0:
        return radius + dist
    else:
        return radius - dist


class TrackPiece(object):
    def __init__(self, index, data, lane_data):
        self.index = index
        self.is_straight = 'length' in data
        self.switch = 'switch' in data
        if self.is_straight:
            self.lengths = [data['length'] for lane in lane_data]
            self.target_speeds = [1000.0 for lane in lane_data]
        else:
            self.lengths = [lane_length(lane['distanceFromCenter'], data['radius'], data['angle']) for lane in lane_data]
            self.radius = data['radius']
            self.lane_radiuses = [true_radius(lane['distanceFromCenter'], self.radius, data['angle']) for lane in lane_data]
            self.target_speeds = [self._initial_target_speed(true_radius(lane['distanceFromCenter'], self.radius, data['angle'])) for lane in lane_data] # base assumption. todo: lane, if success then adjust again
        self.analyzers = [LaneAnalyzer(self, lane['index']) for lane in lane_data]

    def _initial_target_speed(self, radius):

        if radius <= 70:
            return 2.8 + radius / 25.0
        elif radius < 150:
            return 3.2 + radius / 25.0
        else:
            return 7.0 + radius / 65.0

    def __repr__(self): 
        if self.is_straight:
            return "<%d S l=%d>" % (self.index, self.lengths[0])
        else:
            return "<%d B, r=%d, l=%s>" % (self.index, self.radius, ','.join(("%.1f" % length) for length in self.lengths))

