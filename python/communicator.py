import json
import socket
import sys
from botlogger import BotLogger

class Communicator(object):

    def __init__(self, socket, name, key, log_prefix=""):
        self.log_prefix = log_prefix
        if self.log_prefix:
            self.log_prefix += "_"
        self.msglog = BotLogger(self.log_prefix+'messages.log')
        self.actionlog = BotLogger(self.log_prefix+'actions.log', 1000)
        self.socket = socket
        self.name = name
        self.key = key
        self.ai = None
        self.track = None
        self.echo = True


    def set_ai(self, ai):
        self.ai = ai

    def get_own_car(self, data):
        for car in data:
            if car['id']['name'] == self.name:
                return car

    def msg(self, msg_type, data, tick=None):
        message = {"msgType": msg_type, "data": data}
        if tick is not None:
            message['gameTick'] = tick
        self.send(json.dumps(message))

    def send(self, msg):
        self.socket.send(msg + "\n")
        self.msglog.write(msg)

    def join(self):
        return self.msg("join", {"name": self.name, "key": self.key})

    def join_race(self, race):
        return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName":race, "carCount":1})

    def throttle(self, throttle, tick):
        self.msg("throttle", throttle, tick)

    def switch(self, direction):
        self.msg("switchLane", direction)

    def turbo(self):
        self.msg("turbo", "Hear me roar!")

    def ping(self):
        self.msg("ping", {})

    def run(self, race=None):
        if race:
            self.join_race(race)
        else:
            self.join()
        self.msg_loop()


    def on_game_init(self, msg):
        print("* Game init")
        self.ai.init_race(msg['data']['race'])
        #self.ping()

    def on_join(self, msg):
        print("* Joined")
        #self.ping()

    def on_game_start(self, msg):
        print("* Race started")
        self.ping()

    def log_throttle(self, log_action):
        piece = self.ai.current_piece()
        angle, angle_diff = self.ai.get_slide()
        return "%04d P%02d-%s  %.2f %s  %.1f %.1f  %s" % (
                  self.ai.tick, 
                  piece.index, 
                  "S" if piece.is_straight else "B", 
                  self.ai.get()['speed'], 
                  log_action,
                  angle,
                  angle_diff,
                  self.ai.message)

    def on_car_positions(self, msg):
        self.ai.update(msg)
        switch = self.ai.suggest_switch()
        log_action = '???'
        if switch:
            self.switch(switch)
            log_action = "SW" + switch[0]
        elif self.ai.suggest_turbo():
            self.turbo()
            log_action = "TUR"
        else:
            throttle = self.ai.suggest_throttle()
            self.throttle(throttle, self.ai.tick)
            log_action = "%.1f" % throttle

        self.actionlog.write(self.log_throttle(log_action))
        
    def on_crash(self, msg):
        if msg['data']['name'] == self.name:
            delta = self.ai.get_accel("angle")
            slide = self.ai.get()["angle"]
            lane = self.ai.get_lane()
            self.actionlog.write("*** CRASH ***. Slide %.1f, Delta %.1f" % (slide, delta), priority=True)
            self.ai.current_piece().analyzers[lane].crash()
            self.echo = False
        #self.ping()

    def on_spawn(self, msg):
        if msg['data']['name'] == self.name:
            self.actionlog.write("*** SPAWN ***", priority=True)
            self.echo = True
        #self.ping()

    def on_turbo_available(self, msg):
        self.actionlog.write("*** Turbo available", priority=True)
        self.ai.turbo = msg['data']
        #self.ping()

    def on_game_end(self, msg):
        print("Race ended")
        #self.ping()

    def on_tournament_end(self, msg):
        self.ai.dump_ai(self.log_prefix)
        #self.ping()

    def on_error(self, msg):
        self.actionlog.write("Error: {0}".format(msg['data']), priority=True)
        self.ping()



    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'turboAvailable': self.on_turbo_available,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournament_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            self.msglog.write(line)
            msg = json.loads(line)
            msg_type = msg['msgType']
            if msg_type in msg_map:
                msg_map[msg_type](msg)
            else:
                print("! Unhandled message '{0}'".format(msg_type))
                self.ping()
            line = socket_file.readline()

